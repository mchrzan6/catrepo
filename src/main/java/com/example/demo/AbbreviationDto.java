package com.example.demo;


public class AbbreviationDto {
    private String city;
    private String abbreviation;

    public AbbreviationDto(String city, String abbreviation) {
        this.city = city;
        this.abbreviation = abbreviation;
    }

    public AbbreviationDto() {
        this.city = city;
        this.abbreviation = abbreviation;
    }


    public String getCity() {
        return city;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    @Override
    public String toString() {
        return "AbbreviationDto{" +
                "city='" + city + '\'' +
                ", abbreviation='" + abbreviation + '\'' +
                '}';
    }
}
