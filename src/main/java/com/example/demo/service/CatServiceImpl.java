package com.example.demo.service;

import com.example.demo.AbbreviationDto;
import com.example.demo.Cat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Arrays;
import java.util.List;

@Service
public class CatServiceImpl implements CatService {

    private final RestTemplate restTemplate;

    @Autowired
    public CatServiceImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public List<Cat> getCats() {
        sendRequest();
        return Arrays.asList(new Cat("1", "1"), new Cat("2", "2"));
    }

    private void sendRequest() {
        AbbreviationDto abbreviationDto = restTemplate.getForObject(createUri(), AbbreviationDto.class);
        System.out.println(abbreviationDto);
    }

    private String createUri() {
        return UriComponentsBuilder
                .fromHttpUrl("https://gateway-city-app.cfapps.io/gateway")
                .queryParam("city", "cat")
                .build()
                .encode().toUriString();
    }
}
