package com.example.demo.service;

import com.example.demo.Cat;

import java.util.List;

public interface CatService {
    List<Cat> getCats();
}
