package com.example.demo.controller;

import com.example.demo.Cat;
import com.example.demo.service.CatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RequestMapping(value = "/cats")
@RestController
public class CatController {
    private final CatService catService;

    @Autowired
    public CatController(CatService catService) {
        this.catService = catService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<Cat> getCats() {
        return catService.getCats();
    }

    @RequestMapping(method = RequestMethod.POST)
    public List<Cat> createCat(@RequestBody Cat cat) {
        System.out.println(cat);
        List<Cat> cats = new ArrayList<>(catService.getCats());
        cats.add(cat);
        return cats;
    }
}
