package com.example.demo;

public class Cat {
    private String name;
    private String size;

    @Override
    public String toString() {
        return "Cat{" +
                "name='" + name + '\'' +
                ", size='" + size + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }


    public Cat(){

    }

    public Cat(String name, String size) {
        this.name = name;
        this.size = size;
    }
}
